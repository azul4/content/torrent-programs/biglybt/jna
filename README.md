# jna

JNA provides Java programs easy access to native shared libraries without writing anything but Java code - no JNI or native code is required.

https://github.com/java-native-access/jna

<br><br>

How to clone this repository:

```
git clone https://gitlab.com/azul4/content/torrent-programs/biglybt/jna.git
```

